//this file is responsible for submitting the table name from the form
angular.module('home', []).controller('FormCtrl', function($scope, $state) {
  $scope.submit = function() {
    $state.go('table', {
      tableName: $scope.form.tableName,
      userName: $scope.form.userName
    });
  };
});
