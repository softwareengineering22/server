//this file handles all clientside scripting for the table page
angular.module('table', []).controller('TableCtrl', function($scope, $stateParams, tableService, socket) {
  tableService.getTable($stateParams.tableName, $stateParams.userName)
  .then(function(response) {
    buildTable(response.data, new CanvasState(document.getElementById('canvas')));
    socket.emit('roomJoin', response.data.name);
  }, function(error) {
    console.error(error.data.message);
  });

  socket.on('fixDeckCounters', function() {
    var counter = 1;
    $scope.state.entities.forEach(function(entity) {
      if(entity.zone == "deck" && entity.player ==  "opponent") {
        entity.counter = counter;
        counter++;
      }
    });
    $scope.state.valid = false;
  });

  socket.on('moveCard', function(data) {
    for(var i = 0; i < $scope.state.entities.length; i++) {
      if($scope.state.entities[i].id == data.id) {
        //move the selected element to the end of the array so that it's drawn on top
        $scope.state.entities.push($scope.state.entities.splice(i, 1)[0]);
        break;
      }
    }
    var lastIndex = $scope.state.entities.length - 1;

    var canvas = document.getElementById('canvas');
    $scope.state.entities[lastIndex].x = data.newX;
    //reflect over y = canvas.height / 2 because all coordinates received through moveCard event should be from opponent's state
    $scope.state.entities[lastIndex].y = canvas.height - data.newY - $scope.cardHeight;
    var oldZone = $scope.state.entities[lastIndex].zone;
    if(data.newZone != undefined) {
      $scope.state.entities[lastIndex].zone = data.newZone;
    }

    if(!data.facedown) {
      var img = new Image();
      img.onload = function() {
        $scope.state.entities[lastIndex].image = img;
        $scope.state.valid = false;
      }
      img.src = "http://gatherer.wizards.com/Handlers/Image.ashx?multiverseid=" + $scope.state.entities[lastIndex].multiverseId + "&type=card";
    }
    if(oldZone == 'deck' && data.newZone != 'deck') {
      $scope.state.entities[lastIndex].counter = 0;
    }
    if(data.newZone == 'hand' || data.newZone == 'deck') {
      if(data.newZone == 'deck') {
        var counter = 1;
        $scope.state.entities.forEach(function(entity) {
          if(entity.zone == "deck" && entity.player ==  "opponent") {
            entity.counter = counter;
            counter++;
          }
        });
        $scope.state.valid = false;
      }
      var img = new Image();
      img.onload = function() {
        $scope.state.entities[lastIndex].image = img;
        $scope.state.entities[lastIndex].tapped = false;
        $scope.state.valid = false;
      }
      img.src = "table/images/back.jpg";
    }
    else if(data.newZone == 'graveyard' || data.newZone == 'exile') {
      $scope.state.entities[lastIndex].tapped = false;
      $scope.state.valid = false;
    }
    $scope.state.valid = false;
  });

  socket.on('setLife', function(data) {
    var entityIndex;
    for(var i = 0; i < $scope.state.entities.length; i++) {
      if($scope.state.entities[i].id == data.id) {
        entityIndex = i;
        break;
      }
    }

    var newImage = createLifeCounter(data.newLife);
    $scope.state.entities[entityIndex].image = newImage;
    $scope.state.valid = false;
  });

  socket.on('tapCard', function(data) {
    var cardIndex;
    for(var i = 0; i < $scope.state.entities.length; i++) {
      if($scope.state.entities[i].id == data.id) {
        cardIndex = i;
        break;
      }
    }

    var canvas = document.getElementById('canvas');
    $scope.state.entities[cardIndex].tapped = data.tapped;
    $scope.state.valid = false;
  });

    socket.on('setCount', function(data) {
    var cardIndex;
    for(var i = 0; i < $scope.state.entities.length; i++) {
      if($scope.state.entities[i].id == data.id) {
        cardIndex = i;
        break;
      }
    }

    $scope.state.entities[cardIndex].counter = data.counter;
    $scope.state.valid = false;
	});


  socket.on('flipCard', function(data) {
    var cardIndex;
    for(var i = 0; i < $scope.state.entities.length; i++) {
      if($scope.state.entities[i].id == data.id) {
        cardIndex = i;
        break;
      }
    }

    var canvas = document.getElementById('canvas');
    $scope.state.entities[cardIndex].facedown = data.facedown;


    if(!data.facedown) {
      var img = new Image();
      img.onload = function() {
        $scope.state.entities[cardIndex].image = img;
        $scope.state.valid = false;
      }
      img.src = "http://gatherer.wizards.com/Handlers/Image.ashx?multiverseid=" + $scope.state.entities[cardIndex].multiverseId + "&type=card";
    }
    else{
      var img = new Image();
      img.onload = function() {
        $scope.state.entities[cardIndex].image = img;
        $scope.state.valid = false;
      }
      img.src = "table/images/back.jpg"
    }
  });

  $scope.recievedMessage = [];
  $scope.sendMessage = function(message) {
    $scope.recievedMessage.push($stateParams.userName + ": " + message);

    socket.emit('sendMessage', $scope.recievedMessage);
    setTimeout(function() { scrollBar(); }, 50);
  };

  socket.on('sendMessage', function(message) {
    $scope.recievedMessage = message;
    setTimeout(function() { scrollBar(); }, 50);

    var bool = document.getElementById("textBox");
    var button = document.getElementById("hideShowChat");

    if(bool.style.opacity == 1) {
      button.style.background = "white";
    }
    else {
      button.style.background = "yellow";
    }
  });

  //used for keeping the chat scroll bar at the bottom when sending a message
  function scrollBar() {
    var scroll = document.getElementById('textBox');
    scroll.scrollTop = scroll.scrollHeight - scroll.clientHeight;
  }

  //Prevents default right click menu for custom context menu
  $(document).bind("contextmenu", function (event) {
      event.preventDefault();
  });


  //Hides the custom context menu if clicked outside it
  $(document).bind("mousedown", function (e) {
      if (!$(e.target).parents(".customMenu").length > 0) {
          $(".customMenu").hide(100);
          $("#shuffle").finish().css({display: "none"});
          $("#displayDeck").finish().css({display: "none"});
          $("#unDisplayDeck").finish().css({display: "none"});
          $("#addCounter").finish().css({display: "none"});
          $("#removeCounter").finish().css({display: "none"});
          $("#flipOver").finish().css({display: "none"});
          $("#untapAll").finish().css({display: "none"});
      }
  });


  // If a menu element is clicked
  $(".customMenu li").click(function(){
      // This is the triggered action name
      switch($(this).attr("data-action")) {
          // A case for each action. Your actions here
          case "shuffle": shuffleDeck(); break;
          case "addCounter": addCardCounter(); break;
          case "removeCounter": removeCardCouter(); break;
          case "untapAll": untapAllCards(); break;
          case "displayDeck": displayAllCardsInDeck(); break;
          case "unDisplayDeck": unDisplayAllCardsInDeck(); break;
          case "flipOver": flipOverCard(); break;
      }
    // Hide after element is clicked
    $(".customMenu").hide(100);
    $("#shuffle").finish().css({display: "none"});
    $("#displayDeck").finish().css({display: "none"});
    $("#unDisplayDeck").finish().css({display: "none"});
    $("#addCounter").finish().css({display: "none"});
    $("#removeCounter").finish().css({display: "none"});
    $("#flipOver").finish().css({display: "none"});
    $("#untapAll").finish().css({display: "none"});
  });

  var cardFlipX = {};
  var cardFlipY = {};
  function flipOverCard() {
    var entities = $scope.state.entities;
    x = cardFlipX;
    y = cardFlipY;
    // Goes through all cards to see which one you clicked
    for(var i = entities.length - 1; i >= 0; i--) {
      if(entities[i].contains(x, y) && entities[i].moveable && entities[i].zone != 'life') {
        var selection = entities[i];
        $scope.state.selection = selection;
        //check to see if the card is currently loaded facedown
        if($scope.state.selection.image.src.includes('http://gatherer.wizards.com/Handlers/Image.ashx?multiverseid=' + $scope.state.selection.multiverseId + '&type=card')) {
          var img = new Image();
          img.onload = function() {
            $scope.state.selection.image = img;
            $scope.state.valid = false;
          }
          img.src = "table/images/back.jpg";
          $scope.state.selection.facedown = true;
          tableService.flipCard($stateParams.tableName, $stateParams.userName, $scope.state.selection.id, $scope.state.selection.facedown)
          .then(function(response) {
            socket.emit('flipCard', {'id': $scope.state.selection.id, 'facedown': $scope.state.selection.facedown});
          }, function(error) {
            console.error('Error updating card state in database');
          });
          break;
        }
        else {
          var img = new Image();
          img.onload = function() {
            $scope.state.selection.image = img;
            $scope.state.valid = false;
          }
          img.src = "http://gatherer.wizards.com/Handlers/Image.ashx?multiverseid=" + $scope.state.selection.multiverseId + "&type=card";
          $scope.state.selection.facedown = false;
          tableService.flipCard($stateParams.tableName, $stateParams.userName, $scope.state.selection.id, $scope.state.selection.facedown)
          .then(function(response) {
            socket.emit('flipCard', {'id': $scope.state.selection.id, 'facedown': $scope.state.selection.facedown});
          }, function(error) {
            console.error('Error updating card state in database');
          });
          break;
        }
      }
    }
  }

function untapAllCards() {
  var entities = $scope.state.entities;
  for(var i = entities.length - 1; i >= 0; i--){
    if(entities[i].zone == "battlefield" && entities[i].player == "player" && entities[i].tapped) {
      entities[i].tapped = false;
      entID = entities[i].id;
      entTapped = entities[i].tapped;
      $scope.state.valid = false;

      socket.emit('tapCard', {'id': entID, 'tapped': entTapped});
      tableService.tapCard($stateParams.tableName, $stateParams.userName, entities[i].id, entities[i].tapped)
      .then(function(response) {
      }, function(error) {
        console.error('Error updating card tapped in database');
      });
      }
    }
  }

  //this object is defined to keep track of canvas entities and their properties
  var Entity = function(moveable, id, multiverseId, image, x, y, width, height, player, zone, tapped, facedown, counter) {
    this.moveable = moveable;
    this.id = id;
    this.multiverseId = multiverseId;
    this.image = image;
    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;
    this.player = player;
    this.zone = zone;
    this.tapped = tapped;
    this.facedown = facedown;
    this.counter = counter;
  };


  function shuffleDeck() {
    var cardIndexes = [];
    var x = 10;
    var y = canvas.height / 2 + 30;
    var loadCount = 0;
    var counter = 1;
    $scope.state.entities.forEach(function(entity, index) {
      if(entity.zone == "deck" && entity.player ==  "player") {
        cardIndexes.push(index);
      }
    });
    console.log('This is the original cards: ' + cardIndexes);

    cardIndexes.forEach(function(index, indexIndex) {
      $scope.state.entities[index].x = x;
      $scope.state.entities[index].y = y;
      $scope.state.entities[index].counter = indexIndex + 1;

      var currentIndex = cardIndexes.length;
      randomIndex = Math.floor(Math.random() * currentIndex);

      temp = cardIndexes[currentIndex - 1];
      cardIndexes[currentIndex - 1] = cardIndexes[randomIndex];
      cardIndexes[randomIndex] = temp;

      console.log('This is cards after shuffle: ' + cardIndexes);

      if(indexIndex == (cardIndexes.length - 1)) {
        $scope.state.entities[index].moveable = true;
      }
      else {
        $scope.state.entities[index].moveable = false;
      }
      var img = new Image();
      img.onload = function() {
        loadCount++;
        if(loadCount == cardIndexes.length) {
          $scope.state.valid = false;
        }
      }
      img.src = "table/images/back.jpg";
      $scope.state.entities[index].image = img;
      x += .15;
      y += .15;
    });
    $scope.state.entities.forEach(function(entity, index) {
      if(entity.zone == "deck" && entity.player ==  "player") {
        entity.counter = counter;
        counter++;      
      }
    });
    $scope.$apply(function() {
      $scope.sendMessage("I just shuffled my deck.");
    });
  }

  function displayAllCardsInDeck() {
    var cardIndexes = [];
    var x = $scope.cardWidth + 60;
    var y = 1;
    var loadCount = 0;
    $scope.state.entities.forEach(function(entity, index) {
      if(entity.zone == "deck" && entity.player ==  "player") {
        cardIndexes.push(index);
      }
    });
    cardIndexes.forEach(function(index) {
      $scope.state.entities[index].x = x;
      $scope.state.entities[index].y = y;
      $scope.state.entities[index].moveable = true;
      var img = new Image();
      img.onload = function() {
        loadCount++;
        if(loadCount == cardIndexes.length) {
          $scope.state.valid = false;
        }
      }
      img.src = "http://gatherer.wizards.com/Handlers/Image.ashx?multiverseid=" + $scope.state.entities[index].multiverseId + "&type=card";
      $scope.state.entities[index].image = img;
      y += 15;
    });
    $scope.$apply(function() {
      $scope.sendMessage("I am searching my deck");
    });
  }

    function addCardCounter() {
      var entities = $scope.state.entities;
      x = $scope.mouse.x;
      y = $scope.mouse.y;
      // Goes through all cards to see which one you clicked
      for(var i = entities.length - 1; i >= 0; i--) {
        if(entities[i].contains(x, y) && entities[i].moveable && entities[i].zone != 'life') {
          var selection = entities[i];
      		selection.counter = selection.counter+1;
      		$scope.state.valid = false;

          tableService.setCounter($stateParams.tableName, $stateParams.userName, selection.id, selection.counter)
          .then(function(response) {
            socket.emit('setCount', {'id': selection.id, 'counter': selection.counter});
          }, function(error) {
            console.error('Error updating card counter in database');
          });
    	  }
    	}
    }

    function removeCardCouter() {
      var entities = $scope.state.entities;
      x = $scope.mouse.x;
      y = $scope.mouse.y;
      // Goes through all cards to see which one you clicked
      for(var i = entities.length - 1; i >= 0; i--) {
        if(entities[i].contains(x, y) && entities[i].moveable && entities[i].zone != 'life') {
          var selection = entities[i];
      		selection.counter = selection.counter-1;
      		$scope.state.valid = false;

          tableService.setCounter($stateParams.tableName, $stateParams.userName, selection.id, selection.counter)
          .then(function(response) {
            socket.emit('setCount', {'id': selection.id, 'counter': selection.counter});
          }, function(error) {
            console.error('Error updating card counter in database');
          });
    	  }
    	}
    }

  function unDisplayAllCardsInDeck() {
    var cardIndexes = [];
    var x = 10;
    var y = canvas.height / 2 + 30;
    var loadCount = 0;
    $scope.state.entities.forEach(function(entity, index) {
      if(entity.zone == "deck" && entity.player ==  "player") {
        cardIndexes.push(index);
      }
    });
    cardIndexes.forEach(function(index, indexIndex) {
      $scope.state.entities[index].x = x;
      $scope.state.entities[index].y = y;
      $scope.state.entities[index].counter = indexIndex + 1;
      if(indexIndex == (cardIndexes.length - 1)) {
        $scope.state.entities[index].moveable = true;
      }
      else {
        $scope.state.entities[index].moveable = false;
      }
      var img = new Image();
      img.onload = function() {
        loadCount++;
        if(loadCount == cardIndexes.length) {
          $scope.state.valid = false;
        }
      }
      img.src = "table/images/back.jpg";
      $scope.state.entities[index].image = img;
      x += .15
      y += .15;
    });
    socket.emit('fixDeckCounters');
    $scope.$apply(function() {
      $scope.sendMessage("I am done searching my deck");
    });
  }

  $scope.submit = function() {
    var file = document.getElementById('file').files[0];
    var fileReader = new FileReader();
    fileReader.onloadend = function(e) {
      tableService.uploadDeck($stateParams.tableName, $stateParams.userName, e.target.result);
    }
    fileReader.readAsText(file);
  };

  //*******************************************************
  //Everything below is for drawing and managing the canvas
  //*******************************************************

  //this object is defined to keep track of canvas entities and their properties
  var Entity = function(moveable, id, multiverseId, image, x, y, width, height, player, zone, tapped, facedown, counter) {
    this.moveable = moveable;
    this.id = id;
    this.multiverseId = multiverseId;
    this.image = image;
    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;
    this.player = player;
    this.zone = zone;
    this.tapped = tapped;
    this.facedown = facedown;
    this.counter = counter;
  };

  Entity.prototype.draw = function(context) {
    if(this.tapped) {
      //rotate by the top left of the card and then draw it by its new top left (original bottom left)
      context.translate(this.x, this.y);
      context.rotate(Math.PI / 2);
      context.drawImage(this.image, 0, -this.height, this.width, this.height);
      if(this.counter != undefined && this.counter != 0 && (this.zone == 'battlefield' || this.zone == 'deck')) {
        context.drawImage(createCounter(this.counter), this.width - 20, -this.height/2);
      }
      context.rotate(-Math.PI / 2);
      context.translate(-(this.x), -(this.y));
    }
    else {
      context.drawImage(this.image, this.x, this.y, this.width, this.height);
      if(this.counter != undefined && this.counter != 0 && (this.zone == 'battlefield')) {
        context.drawImage(createCounter(this.counter), this.x + this.width - 20, this.y + this.height/2);
      }
      else if(this.zone == 'deck') {
        context.drawImage(createCounter(this.counter), this.x + this.width/2 - 10, this.y + this.height/2);
      }
    }
  };

  // check if the mouse is contained within a card
  Entity.prototype.contains = function(mx, my) {
    if(this.tapped) {
      return  (this.x <= mx) && (this.x + this.height >= mx) && (this.y <= my) && (this.y + this.width >= my);
    }
    else {
      return  (this.x <= mx) && (this.x + this.width >= mx) && (this.y <= my) && (this.y + this.height >= my);
    }
  };

  //this object keeps track of the canvas state and manages events
  var CanvasState = function(canvas) {
    this.canvas = canvas;
    this.width = canvas.width;
    this.height = canvas.height;
    this.context = canvas.getContext('2d');
    this.context.clearRect(0, 0, this.width, this.height);

    // this section handles borders and padding for getting the proper mouse coordinates
    var stylePaddingLeft, stylePaddingTop, styleBorderLeft, styleBorderTop;
    if(document.defaultView && document.defaultView.getComputedStyle) {
      this.stylePaddingLeft = parseInt(document.defaultView.getComputedStyle(canvas, null)['paddingLeft'], 10)      || 0;
      this.stylePaddingTop  = parseInt(document.defaultView.getComputedStyle(canvas, null)['paddingTop'], 10)       || 0;
      this.styleBorderLeft  = parseInt(document.defaultView.getComputedStyle(canvas, null)['borderLeftWidth'], 10)  || 0;
      this.styleBorderTop   = parseInt(document.defaultView.getComputedStyle(canvas, null)['borderTopWidth'], 10)   || 0;
    }
    var html = document.body.parentNode;
    this.htmlTop = html.offsetTop;
    this.htmlLeft = html.offsetLeft;

    this.valid = false; // when set to false, the canvas will redraw everything
    this.entities = [];  // the collection of things to be drawn
    this.dragging = false; // Keep track of when we are dragging
    this.selection = null; // the current selected object
    //offsets for mouse selection
    this.dragoffx = 0;
    this.dragoffy = 0;

    var state = this;

    //fixes a problem where double clicking causes text to get selected on the canvas
    canvas.addEventListener('selectstart', function(e) { e.preventDefault(); return false; }, false);
    // mousedown, mousemove, and mouseup all trigger when dragging
    canvas.addEventListener('mousedown', function(e) {
      if(e.button == 0) {
        var mouse = state.getMouse(e);
        $scope.mouseDown = mouse;
        $scope.deltaX = 0;
        $scope.deltaY = 0;
        var mx = mouse.x;
        var my = mouse.y;

        var entities = state.entities;
        for(var i = entities.length - 1; i >= 0; i--) {
          //check not in zone life because we don't want to drag life counters
          if(entities[i].contains(mx, my) && entities[i].moveable && entities[i].zone != 'life') {
            var selection = entities[i];
            //move the selected element to the end of the array so that it's drawn on top
            entities.push(entities.splice(i, 1)[0]);
            // Keep track of where in the object we clicked
            // so we can move it smoothly (see mousemove)
            state.dragoffx = mx - selection.x;
            state.dragoffy = my - selection.y;
            state.dragging = true;
            state.selection = selection;
            state.valid = false;
            return;
          }
        }
        // havent returned means we have failed to select anything.
        // If there was an object selected, we deselect it
        if(state.selection) {
          state.selection = null;
          state.valid = false;
        }
      }
    }, true);

    document.addEventListener('keydown', function(e) {
      // if the left control key is pressed, we want to zoom the card that the mouse is under
      if(e.keyCode == 17) {
        var entities = state.entities;
        var mouse = $scope.mouse;
        var mx = mouse.x;
        var my = mouse.y;
        for(var i = entities.length - 1; i >= 0; i--) {
          if(entities[i].contains(mx, my) && entities[i].zone == "battlefield") {
            // if we're moving from a card directly to another card without space between
            if($scope.zoomed != undefined && entities[i] != $scope.zoomed) {
              // if the player was looking at a facedown card it is zoomed faceup, unzoom it to facedown
              if($scope.zoomed.player == 'player' && $scope.zoomed.facedown) {
                var img = new Image();
                img.onload = function() {
                  $scope.unzoomed.width = 223 / 2;
                  $scope.unzoomed.height = 311 / 2;
                  $scope.unzoomed.image = img;
                  $scope.state.valid = false;
                }
                img.src = "table/images/back.jpg";
                // keep track of the card we moved from for the onload event
                $scope.unzoomed = $scope.zoomed;
              }
              // simple unzoom and clear $scope.zoomed
              else {
                $scope.zoomed.width = 223 / 2;
                $scope.zoomed.height = 311 / 2;
                state.valid = false;
                $scope.zoomed = undefined;
              }
            }
            // if the card is the player's and facedown, zoom it faceup so the player can see what card it is
            if(entities[i].player == 'player' && entities[i].facedown) {
              // use a second image because it was causing issues moving from one player's facedown card to another
              var img2 = new Image();
              img2.onload = function() {
                $scope.zoomed.width = 223;
                $scope.zoomed.height = 311;
                $scope.zoomed.image = img2;
                // move the selected element to the end of the array so that it's drawn on top
                entities.push(entities.splice(i, 1)[0]);
                $scope.state.valid = false;
              }
              img2.src = "http://gatherer.wizards.com/Handlers/Image.ashx?multiverseid=" + entities[i].multiverseId + "&type=card";
              // keep track of which card is zoomed for unzooming, etc.
              $scope.zoomed = entities[i];
            }
            // simple zoom when not moving to a player's card that's facedown
            else {
              $scope.zoomed = entities[i];
              $scope.zoomed.width = 223;
              $scope.zoomed.height = 311;
              // move the selected element to the end of the array so that it's drawn on top
              entities.push(entities.splice(i, 1)[0]);
              state.valid = false;
            }
            break;
          }
          // if moving from a zoomed card to nothing(checked by i == 0 means whole list is scanned), unzoom
          else if($scope.zoomed != undefined && i == 0) {
            // if moving off a player's card that is supposed to be facedown
            if($scope.zoomed.player == 'player' && $scope.zoomed.facedown) {
              var img = new Image();
              img.onload = function() {
                $scope.zoomed.width = 223 / 2;
                $scope.zoomed.height = 311 / 2;
                $scope.zoomed.image = img;
                $scope.state.valid = false;
                $scope.zoomed = undefined;
              }
              img.src = "table/images/back.jpg";
            }
            // simple unzoom
            else {
              $scope.zoomed.width = 223 / 2;
              $scope.zoomed.height = 311 / 2;
              state.valid = false;
              $scope.zoomed = undefined;
            }
          }
        }
      }
    }, true);

    document.addEventListener('keyup', function(e) {
      // if the left control key is no longer pressed, we want to unzoom all cards
      if(e.keyCode == 17) {
        if($scope.zoomed != undefined) {
          if($scope.zoomed.player == 'player' && $scope.zoomed.facedown && !$scope.zoomed.image.src.includes('back.jpg')) {
            var img = new Image();
            img.onload = function() {
              $scope.zoomed.width = 223 / 2;
              $scope.zoomed.height = 311 / 2;
              $scope.zoomed.image = img;
              $scope.state.valid = false;
              $scope.zoomed = undefined;
            }
            img.src = "table/images/back.jpg";
          }
          else {
            $scope.zoomed.width = 223 / 2;
            $scope.zoomed.height = 311 / 2;
            state.valid = false;
            $scope.zoomed = undefined;
          }
        }
      }
    }, true);

    canvas.addEventListener('mousemove', function(e) {
      //cache mouse location for keyboard events
      $scope.mouse = state.getMouse(e);
      if(state.dragging) {
        var mouse = state.getMouse(e);
        // We don't want to drag the object by its top-left corner, we want to drag it
        // from where we clicked. Thats why we saved the offset and use it here
        state.selection.x = mouse.x - state.dragoffx;
        state.selection.y = mouse.y - state.dragoffy;
        //emit move event whenver card moves so that the opponent can see it dragging
        socket.emit('moveCard', {'id': state.selection.id, 'newX': state.selection.x, 'newY': state.selection.y, 'facedown': true});
        state.valid = false; // Something's dragging so we must redraw
      }
    }, true);

    canvas.addEventListener('mouseup', function(e) {
      if(e.button == 0) {
        state.dragging = false;
        var mouse = state.getMouse(e);
        //deltas used to make sure that we fire click events when clicked and move events when truly moved
        $scope.deltaX = Math.abs($scope.mouseDown.x - mouse.x);
        $scope.deltaY = Math.abs($scope.mouseDown.y - mouse.y);
        if(state.selection && $scope.deltaX > 1 && $scope.deltaY > 1) {
          state.selection.x = mouse.x - state.dragoffx;
          state.selection.y = mouse.y - state.dragoffy;
          var canvas = document.getElementById('canvas');
          var oldZone;
          //check if the selected card is from the deck to make the next card interactable
          if(state.selection.zone == 'deck') {
            oldZone = "deck";
            $scope.playerDeckFound--;
            state.selection.counter = 0;
            state.valid = false;
            for(var i = state.entities.length - 1; i >= 0; i--) {
              if(state.entities[i].zone == 'deck' && state.entities[i] != state.selection && state.entities[i].player == 'player') {
                state.entities[i].moveable = true;
                break;
              }
            }
          }
          //check if the card is going to drop into the hand
          if(mouse.x < (canvas.width - $scope.cardWidth) && mouse.y < canvas.height && mouse.y >= (canvas.height - $scope.cardHeight)) {
            state.selection.zone = 'hand';
            state.selection.counter = 0;
            //check to see if the card is currently loaded facedown
            if(state.selection.image.src.includes('back.jpg')) {
              var img = new Image();
              img.onload = function() {
                state.selection.image = img;
                state.valid = false;
              }
              img.src = "http://gatherer.wizards.com/Handlers/Image.ashx?multiverseid=" + state.selection.multiverseId + "&type=card";
            }
            if(state.selection.tapped) {
              state.selection.tapped = false;
              state.valid = false;
            }
            if(state.selection.facedown) {
              state.selection.facedown = false;
              state.valid = false;
            }
            state.valid = false;
          }
          //check if the card is going to drop into the deck
          else if(mouse.x >= 10 && mouse.x <= (10 + $scope.cardWidth) && mouse.y >= (canvas.height / 2 + 30) && mouse.y <= (canvas.height / 2 + 30 + $scope.cardHeight)) {
            $scope.playerDeckFound++;
            state.selection.counter = $scope.playerDeckFound;
            //need to make the top card of the deck unmovable before putting a card back on the deck
            for(var i = state.entities.length - 1; i >= 0; i--) {
              if(state.entities[i].zone == 'deck' && state.entities[i] != state.selection && state.entities[i].player == 'player') {
                state.entities[i].moveable = false;
                break;
              }
            }
            state.selection.zone = 'deck';
            var img = new Image();
            img.onload = function() {
              state.selection.image = img;
              state.valid = false;
            }
            img.src = "table/images/back.jpg";
            if(state.selection.tapped) {
              state.selection.tapped = false;
              state.valid = false;
            }
            if(state.selection.facedown) {
              state.selection.facedown = false;
              state.valid = false;
            }
            state.valid = false;
          }
          //check if the card is going to drop into the exile
          else if(mouse.x >= canvas.width - $scope.cardWidth && mouse.x <= canvas.width && mouse.y >= (canvas.height / 2 + 30) && mouse.y <= (canvas.height / 2 + 30 + $scope.cardHeight)) {
            state.selection.zone = 'exile';
            state.selection.counter = 0;
            //check to see if the card is currently loaded facedown
            if(state.selection.image.src.includes('back.jpg')) {
              var img = new Image();
              img.onload = function() {
                state.selection.image = img;
                state.valid = false;
              }
              img.src = "http://gatherer.wizards.com/Handlers/Image.ashx?multiverseid=" + state.selection.multiverseId + "&type=card";
            }
            if(state.selection.tapped) {
              state.selection.tapped = false;
              state.valid = false;
            }
            if(state.selection.facedown) {
              state.selection.facedown = false;
              state.valid = false;
            }
            state.valid = false;
          }
          //check if the card is going to drop into the graveyard
          else if(mouse.x >= canvas.width - $scope.cardWidth && mouse.x <= canvas.width && mouse.y >= (canvas.height - $scope.cardHeight - 30) && mouse.y <= (canvas.height - 30)) {
            state.selection.zone = 'graveyard';
            state.selection.counter = 0;
            //check to see if the card is currently loaded facedown
            if(state.selection.image.src.includes('back.jpg')) {
              var img = new Image();
              img.onload = function() {
                state.selection.image = img;
                state.valid = false;
              }
              img.src = "http://gatherer.wizards.com/Handlers/Image.ashx?multiverseid=" + state.selection.multiverseId + "&type=card";
            }
            if(state.selection.tapped) {
              state.selection.tapped = false;
              state.valid = false;
            }
            if(state.selection.facedown) {
              state.selection.facedown = false;
              state.valid = false;
            }
            state.valid = false;
          }
          else {
            state.selection.zone = 'battlefield';
            if(oldZone == "deck") {
              state.selection.counter = 0;
              //default facedown when moving directly from the deck to battlefield
              state.selection.facedown = true;
              // check to see if the card isn't loaded facedown
              if(!state.selection.image.src.includes('back.jpg')) {
                var img = new Image();
                img.onload = function() {
                  state.selection.image = img;
                  state.valid = false;
                }
                img.src = "table/images/back.jpg";
              }
            }
          }
          tableService.moveCard($stateParams.tableName, $stateParams.userName, state.selection.id, state.selection.x, state.selection.y, state.selection.zone, state.selection.facedown)
          .then(function(response) {
            socket.emit('moveCard', {'id': state.selection.id, 'newX': state.selection.x, 'newY': state.selection.y, 'newZone': state.selection.zone, 'facedown': state.selection.facedown});
          }, function(error) {
            console.error('Error updating card location in database');
          });
        }
      }
      else if (e.button == 2) {
        var mouse = state.getMouse(e);
        var canvas = document.getElementById('canvas');
        var entities = state.entities;
        cardFlipX = mouse.x;
        cardFlipY = mouse.y;

        // enables the customMenu
        $(".customMenu").finish().css({top: e.pageY + "px", left: e.pageX + "px", display: "block"});
        // locate the clicked card and set a flag for "zone" (note: "zone" opponent is used but is not a real zone)
        for(var i = entities.length - 1; i >= 0; i--) {
          var selectedZone;
          if(entities[i].contains(mouse.x, mouse.y) && entities[i].player == 'player') {
            selectedZone = entities[i].zone;
            break;
          }
          else if(entities[i].contains(mouse.x, mouse.y) && entities[i].player == 'opponent') {
            selectedZone = 'opponent';
            break;
          }
        }

        if(selectedZone == 'battlefield') {
          $("#addCounter").finish().css({display: "block"});
          $("#removeCounter").finish().css({display: "block"});
          $("#flipOver").finish().css({display: "block"});
          $("#untapAll").finish().css({display: "block"});
        }
        else if(selectedZone == 'deck') {
          $("#shuffle").finish().css({display: "block"});
          $("#displayDeck").finish().css({display: "block"});
          $("#unDisplayDeck").finish().css({display: "block"});
        }
        else if(selectedZone == 'hand') {
          $("#flipOver").finish().css({display: "block"});
        }
        else if(selectedZone == 'graveyard' || selectedZone == 'exile' || selectedZone == 'opponent') {}
        else {
          $("#untapAll").finish().css({display: "block"});
        }
      }
      state.valid = false;
    }, true);
    canvas.addEventListener('click', function(e) {
      //need to make sure that we're not actually dragging
      if($scope.deltaX <= 1 && $scope.deltaY <= 1) {
        var mouse = state.getMouse(e);
        var mx = mouse.x;
        var my = mouse.y;
        var entities = state.entities;
        for(var i = entities.length - 1; i >= 0; i--) {
          if(entities[i].contains(mx, my) && entities[i].moveable) {
            if(entities[i].zone == 'life') {
              //check if click is in the bounding box of the up-life button
              if(mx >= $scope.lifeX && mx <= $scope.lifeX + 150 && my >= $scope.lifeY && my <= $scope.lifeY + 37) {
                $scope.playerLife++;
                var newImage = createLifeCounter($scope.playerLife);
                entities[i].image = newImage;
                state.valid = false;

                tableService.setLife($stateParams.tableName, $stateParams.userName, $scope.playerLife)
                .then(function(response) {
                  //emit the id opponentLife because only the opponent will receive the socket event
                  socket.emit('setLife', {'id': 'opponentLife', 'newLife': $scope.playerLife});
                }, function(error) {
                  console.error('Error updating life in database');
                });
              }
              //check if click is in the bounding box of the down-life button
              else if(mx >= $scope.lifeX && mx <= $scope.lifeX + 150 && my >= $scope.lifeY + 112 && my <= $scope.lifeY + 112 + 37){
                $scope.playerLife--;
                var newImage = createLifeCounter($scope.playerLife);
                entities[i].image = newImage;
                state.valid = false;

                tableService.setLife($stateParams.tableName, $stateParams.userName, $scope.playerLife)
                .then(function(response) {
                  //emit the id opponentLife because only the opponent will receive the socket event
                  socket.emit('setLife', {'id': 'opponentLife', 'newLife': $scope.playerLife});
                }, function(error) {
                  console.error('Error updating life in database');
                });
              }
              return;
            }
            else if(entities[i].zone == 'battlefield') {
              var selection = entities[i];
              if(entities[i].tapped) {
                selection.tapped = false;
              }
              else if(!entities[i].tapped) {
                selection.tapped = true;
              }
              state.selection = selection;
              state.valid = false;

              tableService.tapCard($stateParams.tableName, $stateParams.userName, selection.id, selection.tapped)
              .then(function(response) {
                socket.emit('tapCard', {'id': selection.id, 'tapped': selection.tapped});
              }, function(error) {
                console.error('Error updating card tapped in database');
              });
              return;
            }
          }
        }
        // havent returned means we have failed to select anything.
        // If there was an object selected, we deselect it
        if(state.selection) {
          state.selection = null;
          state.valid = false;
        }
      }
    }, true);
    setInterval(function() { state.draw(); }, 10);
  }

  CanvasState.prototype.addEntity = function(card) {
    this.entities.push(card);
    this.valid = false;
  }

  CanvasState.prototype.clear = function() {
    this.context.clearRect(0, 0, this.width, this.height);
  }

  CanvasState.prototype.draw = function() {
    var originalCardWidth = 223;
    var originalCardHeight = 311;
    var cardWidth = originalCardWidth/2;
    var cardHeight = originalCardHeight/2;
    var pdX = 10;
    var pdY = canvas.height / 2 + 30;

    var peX = canvas.width - cardWidth - 10;
    var peY = canvas.height / 2 + 30;

    var pgX = canvas.width - cardWidth - 10;
    var pgY = canvas.height - cardHeight - 30;

    // if our state is invalid, redraw and validate!
    if(!this.valid) {
      var context = this.context;
      var entities = this.entities;

      this.clear();
      // ** Add stuff you want drawn in the background all the time here **
      //grey-purple
    	context.fillStyle = "#CAC3B4";
    	context.fillRect(canvas.width-150, 0, 150, canvas.height);
    	//top red rect
    	context.fillStyle = "#A53934";
    	context.fillRect(0,0, canvas.width, canvas.height/3-50);
    	//bottom blue rect
    	context.fillStyle = "#232F3B";
    	context.fillRect(0, canvas.height-(canvas.height/3-50), canvas.width, canvas.height/3-50);
    	//tan center
    	context.fillStyle = "#EEB87C";
    	context.fillRect(0, canvas.height/3-50, canvas.width-150, canvas.height/3+100);
    	//red side
    	context.fillStyle = "#A53934";
    	context.fillRect(canvas.width, 0, 150, canvas.height/2);
    	//blue side
    	context.fillStyle = "#232F3B";
    	context.fillRect(canvas.width, canvas.height/2, 150, canvas.height/2);
    	//middle bar
    	context.fillStyle = "#577167";
    	context.fillRect(canvas.width-150, canvas.height/2-10, 150, 5);

      // draw all entities
      for(var i = 0; i < entities.length; i++) {
        var shape = entities[i];
        // We can skip the drawing of elements that have moved off the screen:
        if(shape.x > this.width || shape.y > this.height || shape.x + shape.w < 0 || shape.y + shape.h < 0) continue;
        entities[i].draw(context);
      }
      if(this.dragging){
        context.lineWidth = 3;
        context.strokeStyle = "#6666ff"
        //bottom blue rect
        context.strokeRect(0, canvas.height-(canvas.height/3-50), canvas.width, canvas.height/3-50);
        //tan center
        context.strokeRect(0, canvas.height/3-50, canvas.width-150, canvas.height/3+100);
        //graveyard exile deck
        context.strokeRect(pdX-2, pdY-2, cardWidth+4, cardHeight+5);
        context.strokeRect(pgX-2, pgY-2, cardWidth+4, cardHeight+5);
        context.strokeRect(peX-2, peY-2, cardWidth+4, cardHeight+5);

        for(var i = 0; i < entities.length; i++) {
          var shape = entities[i];
          // We can skip the drawing of elements that have moved off the screen:
          if(shape.x > this.width || shape.y > this.height || shape.x + shape.w < 0 || shape.y + shape.h < 0) continue;
          entities[i].draw(context);
        }
        this.valid = false;
      }
      this.valid = true;
      $scope.state = this;
    }
  };

  // Creates an object with x and y defined, set to the mouse position relative to the state's canvas
  CanvasState.prototype.getMouse = function(e) {
    var element = this.canvas, offsetX = 0, offsetY = 0, mx, my;

    // Compute the total offset
    if(element.offsetParent !== undefined) {
      do {
        offsetX += element.offsetLeft;
        offsetY += element.offsetTop;
      } while ((element = element.offsetParent));
    }

    // Add padding and border style widths to offset
    // Also add the <html> offsets in case there's a position:fixed bar
    offsetX += this.stylePaddingLeft + this.styleBorderLeft + this.htmlLeft;
    offsetY += this.stylePaddingTop + this.styleBorderTop + this.htmlTop;

    mx = e.pageX - offsetX;
    my = e.pageY - offsetY;

    // We return a simple javascript object (a hash) with x and y defined
    return {x: mx, y: my};
  };

  //builds the table from database table
  var buildTable = function(table, canvasState) {
    //this section sets each player up in the correct slot
    var player;
    var opponent;
    if(table.players[0].name == $stateParams.userName) {
      player = table.players[0];
      opponent = table.players[1];
    }
    else if(table.players[1].name == $stateParams.userName) {
      player = table.players[1];
      opponent = table.players[0];
    }

    var canvas = document.getElementById('canvas');
    var originalCardWidth = 223;
    var originalCardHeight = 311;
    var cardWidth = originalCardWidth/2;
    var cardHeight = originalCardHeight/2;
    $scope.cardWidth = cardWidth;
    $scope.cardHeight = cardHeight;

	$scope.screenHeight = canvas.height;
	$scope.screenWidth = canvas.width;




    var playerLifeCounter = createLifeCounter(player.life);
    var opponentLifeCounter = createLifeCounter(opponent.life);
    //used to find the counter when click events are handled
    $scope.lifeX = 10;
    $scope.lifeY = canvas.height / 2 + 250;
    $scope.playerLife = player.life;

	  canvasState.addEntity(new Entity(true, 'playerLife', null, playerLifeCounter, $scope.lifeX, $scope.lifeY, 150, 150, 'player', 'life', false, false));
    //reflect over x=canvasHeight/2 and offset by the height of the counter to place on the opponent's side
	  canvasState.addEntity(new Entity(false, 'opponentLife', null, opponentLifeCounter, 10, (canvas.height / 2 - 150 - 250), 150, 150, 'opponent', 'life', false, false));

    //draw the current player's cards
    var pdX = 10;
    var pdY = canvas.height / 2 + 30;

    var peX = canvas.width - cardWidth - 10;
    var peY = canvas.height / 2 + 30;

    var pgX = canvas.width - cardWidth - 10;
    var pgY = canvas.height - cardHeight - 30;

    //need to get a count of the amount of cards in hand to properly center
    //need to get a count of the amount of cards in deck to set interactability
    var playerHandLength = 0;
    var playerDeckLength = 0;
    player.cards.forEach(function(card) {
      if(card.zone == 'hand') {
        playerHandLength++;
      }
      else if(card.zone == 'deck') {
        playerDeckLength++;
      }
    });
    var phX = canvas.width / 2 - playerHandLength * cardWidth / 2;
    var phY = canvas.height - cardHeight;

    var playerLoadCount = 0;
    var playerImages = [];
    $scope.playerDeckFound = 0;

    player.cards.forEach(function(card, i) {
      var img = new Image();
      img.onload = function() {
        playerLoadCount++;
        if(playerLoadCount == player.cards.length) {
          for(var j = 0; j < playerImages.length; j++) {
            canvasState.addEntity(playerImages[j]);
          }
        }
      }
      if(card.zone == 'deck') {
        $scope.playerDeckFound++;
        img.src = "table/images/back.jpg";
        if($scope.playerDeckFound == playerDeckLength) {
          playerImages.push(new Entity(true, card._id, card.multiverseId, img, pdX, pdY, cardWidth, cardHeight, 'player', card.zone, false, false, $scope.playerDeckFound));
        }
        else {
          playerImages.push(new Entity(false, card._id, card.multiverseId, img, pdX, pdY, cardWidth, cardHeight, 'player', card.zone, false, false, $scope.playerDeckFound));
        }
        pdX += .15;
        pdY += .15;
      }
      else if(card.zone == 'battlefield') {
        if(card.facedown) {
          img.src = "table/images/back.jpg";
        }
        else {
          img.src = "http://gatherer.wizards.com/Handlers/Image.ashx?multiverseid=" + card.multiverseId + "&type=card";
        }

        playerImages.push(new Entity(true, card._id, card.multiverseId, img, card.locX, card.locY, cardWidth, cardHeight, 'player', card.zone, card.tapped, card.facedown, card.counter));
      }
      else if(card.zone == 'exile') {
        img.src = "http://gatherer.wizards.com/Handlers/Image.ashx?multiverseid=" + card.multiverseId + "&type=card";
        playerImages.push(new Entity(true, card._id, card.multiverseId, img, peX, peY, cardWidth, cardHeight, 'player', card.zone, false, false, card.counter));
        peY += 15;
      }
      else if(card.zone == 'graveyard') {
        img.src = "http://gatherer.wizards.com/Handlers/Image.ashx?multiverseid=" + card.multiverseId + "&type=card";
        playerImages.push(new Entity(true, card._id, card.multiverseId, img, pgX, pgY, cardWidth, cardHeight, 'player', card.zone, false, false, card.counter));
        pgY += 15;
      }
      else if(card.zone == 'hand') {
        img.src = "http://gatherer.wizards.com/Handlers/Image.ashx?multiverseid=" + card.multiverseId + "&type=card";
        playerImages.push(new Entity(true, card._id, card.multiverseId, img, phX, phY, cardWidth, cardHeight, 'player', card.zone, false, false, card.counter));
        phX += cardWidth;
      }
    });

    //draw the opponent's cards
    var odX = 10;
    var odY = canvas.height / 2 - cardHeight - 30;

    var oeX = canvas.width - cardWidth - 10;
    var oeY = canvas.height / 2 - cardHeight - 30;

    var ogX = canvas.width - cardWidth - 10;
    var ogY = 10;

    //need to get a count of the amount of cards in hand to properly center
    var opponentHand = 0;
    opponent.cards.forEach(function(card) {
      if(card.zone == 'hand') {
        opponentHand++;
      }
    });
    var ohX = canvas.width / 2 - opponentHand * cardWidth / 2;
    var ohY = 10;

    var opponentLoadCount = 0;
    var opponentImages = [];
    $scope.opponentDeckFound = 0;

    opponent.cards.forEach(function(card, i) {
      var img = new Image();
      img.onload = function() {
        opponentLoadCount++;
        if(opponentLoadCount == opponent.cards.length) {
          for(var j = 0; j < opponentImages.length; j++) {
            canvasState.addEntity(opponentImages[j]);
          }
        }
      }
      if(card.zone == 'deck') {
        $scope.opponentDeckFound++;
        img.src = "table/images/back.jpg";
        opponentImages.push(new Entity(false, card._id, card.multiverseId, img, odX, odY, cardWidth, cardHeight, 'opponent', card.zone, false, false, $scope.opponentDeckFound));
        odX += .15;
        odY += .15;
      }
      else if(card.zone == 'battlefield') {
        if(card.facedown) {
          img.src = "table/images/back.jpg";
        }
        else {
          img.src = "http://gatherer.wizards.com/Handlers/Image.ashx?multiverseid=" + card.multiverseId + "&type=card";
        }

        opponentImages.push(new Entity(false, card._id, card.multiverseId, img, card.locX, (canvas.height - card.locY - cardHeight), cardWidth, cardHeight, 'opponent', card.zone, card.tapped, card.facedown, card.counter));
      }
      else if(card.zone == 'exile') {
        img.src = "http://gatherer.wizards.com/Handlers/Image.ashx?multiverseid=" + card.multiverseId + "&type=card";
        opponentImages.push(new Entity(false, card._id, card.multiverseId, img, oeX, oeY, cardWidth, cardHeight, 'opponent', card.zone, false, false, card.counter));
        oeY += 15;
      }
      else if(card.zone == 'graveyard') {
        img.src = "http://gatherer.wizards.com/Handlers/Image.ashx?multiverseid=" + card.multiverseId + "&type=card";
        opponentImages.push(new Entity(false, card._id, card.multiverseId, img, ogX, ogY, cardWidth, cardHeight, 'opponent', card.zone, false, false, card.counter));
        ogY += 15;
      }
      else if(card.zone == 'hand') {
        img.src = "table/images/back.jpg";
        opponentImages.push(new Entity(false, card._id, card.multiverseId, img, ohX, ohY, cardWidth, cardHeight, 'opponent', card.zone, false, false, card.counter));
        ohX += cardWidth;
      }
    });
  };

  var createCounter = function(value) {
	  //Create a canvas element in memory to draw deck counter
	  var canvas = document.createElement("Canvas");
    var context = canvas.getContext("2d");
	  //set canvas to card height and width
	  canvas.height = 20;
	  canvas.width = 20;

	  context.beginPath();
		context.arc(10, 10, 10, 0, 2*Math.PI);
		context.fillStyle = "#CAC3B4";
		context.fill();

		context.font = 10 + "px arial";
		context.fillStyle = "black";
		context.textBaseline="middle";
		context.textAlign="center";
		context.fillText(value.toString(), 10, 10);

    return canvas;
  }


  var createLifeCounter = function(life) {
    //create a canvas element in memory to draw a life counter
    var canvas = document.createElement("Canvas");
    var context = canvas.getContext("2d");
    canvas.height = 150;
		canvas.width = 150;

		<!--Background rectangle-->
		context.fillStyle = "#A1B38E";
		var rect = {x: 0, y: 37, w: 150, h: 75};
		context.fillRect(rect.x, rect.y, rect.w, rect.h);

		<!-- Upper rectangle-->
		context.fillStyle = "#577167";
		var up = {x: 0, y: 0, w: 150, h: 37};
		context.fillRect(up.x, up.y, up.w, up.h);

		<!--Lower rectangle-->
		context.fillStyle = "#577167";
		var down = {x: 0, y: 112, w: 150, h: 37};
		context.fillRect(down.x, down.y, down.w, down.h);

		<!--Up triangle-->
		context.fillStyle = "#A1B38E";
		context.beginPath();
		context.moveTo(75, 10);
		context.lineTo(65, 25);
		context.lineTo(85, 25);
		context.fill();

		<!--Down triangle-->
		context.fillStyle = "#A1B38E";
		context.beginPath();
		context.moveTo(75, 142);
		context.lineTo(65, 127);
		context.lineTo(85, 127);
		context.fill();

		<!--Display Counter-->
		context.font = 150*0.25 + "px arial";
		context.fillStyle = "black";
		context.textBaseline = "middle";
		context.textAlign = "center";
		context.fillText(life.toString(), 75, 75);

    return canvas;
  }
});
