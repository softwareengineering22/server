//this file handles all clientside scripting for the deckbuilder page
angular.module('builder', []).controller('BuilderCtrl', function($scope) {
  var dragSrcEl = null;
  function handleDragOver(e) {
    if (e.preventDefault) { e.preventDefault(); }
    return false;
  }

  function addOne(deck) {
    var card = $("#cardlist option:selected").data('card');

    if(!card) {
      // nothing selected...
      return false;
    }

    var types = [
      ["Planeswalker", "planeswalkers"],
      ["Creature",     "creatures"],
      ["Artifact",     "artifacts"],
      ["Enchantment",  "enchantments"],
      ["Land",         "lands"],
    ]

    for(var i = 0; i < types.length; i++) {
      if (card.types.indexOf(types[i][0]) >= 0) {
        deck.addCard(card, types[i][1]);
        return false;
      }
    }

    deck.addCard(card, "spells");
    return false;
  }

  function addFour(deck) {
    for(var i = 0; i < 4; i++) { addOne(deck); }
    return false;
  }

  function Storage() { }
  Storage.prototype.read = function() {
    if (window.localStorage.decks) {
      return JSON.parse(window.localStorage.decks);
    } else {
      return JSON.parse('[{"name":"Sample Deck","creatures":[270959,270959,270959,270959,373541,373541,373541,373541,368961,253624,253624,253624,253624,369030,366328,366310,366239,378524],"spells":[373575,373575,373575,373575,370609,370609,373639,373639,373639,373639,373701,366379],"artifacts":[373709,373544,290542],"enchantments":[373715,373715,253507],"lands":[370733,373734,369058,373608,373608,373608,373608,373608,373608,373608,373608,373608,373608,373608,373608,373608,373608,373608,373608,373595,373595,373595,373595,373595,373595,373595,373595],"planeswalkers":[370728]}]');
    }
  }
  Storage.prototype.write = function(data) {
    window.localStorage.decks = JSON.stringify(data);
  }

  var storage = new Storage();

  var mvidToCards = {};

  function addDeckList(savedDecks, form) {
    var select = $("select", form);
    select.empty();

    // Default empty option
    select.append($('<option></option>'));

    $.each(savedDecks, function(i, deck) {
      select.append($('<option></option>')
          .attr('value', deck['name']).text(deck['name']));
    });
  }

  function saveDeck(storage, deck, name) {
    var decks = storage.read();
    var existingDeck = decks.filter(function(storedDeck) {
      return storedDeck.name == name;
    })[0];

    var mvidfun = function(x) { return x.multiverseid; };
    var storedDeck = {}

    if (existingDeck) { storedDeck = existingDeck }

    storedDeck.name = name;
    deck.rowTypes.forEach(function(type) {
      storedDeck[type] = deck[type]().map(mvidfun);
    });

    if (!existingDeck) { decks.push(storedDeck); }
    addDeckList(decks, $("#deck-controls"));
    storage.write(decks);
  }

  function loadDeck(storage, name, table, mvidToCards) {
    var deck = storage.read().filter(function(storedDeck) {
      return storedDeck.name == name;
    })[0];

    var resolved = {};

    for (var section in table.cardColumns) {
      resolved[section] = (deck[section] || []).map(function(mvid) {
        return mvidToCards[mvid];
      });
    }
    table.setDeck(resolved)
  }

  function downloadDecks(deck) {
    var decks = storage.read();
    var lastDeck = decks[decks.length - 1];
    //some bullshit to flatten all the ids
    var cards = _.flatten(
      deck.rowTypes.map(function(type) {
        return lastDeck[type];
      }));
    return {
      "cards": cards
    };
  }

  $(document).ready(function() {
    var deck = new Table($("div.deck")[0]);
    var trash = document.querySelector('div.trash');

    trash.addEventListener('dragover', handleDragOver, false);
    trash.addEventListener('drop', function (e) {
      if (e.stopPropagation) { e.stopPropagation(); }
      deck.removeCard(dragSrcEl);
      return false;
    }, false);

    $("#addone").click(function() { return addOne(deck); });
    $("#addfour").click(function() { return addFour(deck); });
    $("#download").click(function() {
      var download = downloadDecks(deck);
      var dlstr = "data:application/octet-stream;charset=utf-8," +
        encodeURIComponent(JSON.stringify(download));
      this.href = dlstr;
      this.download = "decks.json"
      return true;
    });

    $("#load").click(function() {
      var deckName = $("select", $(this).parents("form")).val();
      if (deckName != '') {
        deck.clear();
        loadDeck(storage, deckName, deck, mvidToCards);
      }
      return false;
    });

    $("#new").click(function() {
      deck.clear();
      var form = $(this).parents("form");
      $("select", form).val('');
      $("input", form).val('');
      return false;
    });

    $("#save").click(function() {
      var form = $(this).parents("form");
      var selectName = $("select", form).val();
      var textName   = $("input", form).val();

      var deckName = null;

      if (selectName == "" && textName == "") {
        deckName = "New Deck";
      } else if (selectName == "" || textName != "") {
        deckName = textName;
      } else {
        deckName = selectName;
      }

      saveDeck(storage, deck, deckName);
      $("select", form).val(deckName);
      $("input", form).val('');
      return false;
    });

    addDeckList(storage.read(), $("#deck-controls"));

    $.getJSON("/builder/content/allsets.json", function(data) {
      var sets = ["AER", "KLD", "EMN", "SOI", "OGW", "BFZ"];
      // for (var key in data) {
      //   if (data.hasOwnProperty(key)) {
      //     sets.push(key);
      //   }
      // }
      var cards = sets.reduce(function(prev, curr) {
        return prev.concat(data[curr].cards);
      }, []).sort(function(a,b) {
        if (a.name > b.name) return 1;
        if (a.name < b.name) return -1;
        return 0;
      });

      var options = $.map(cards, function(card) {
          return $('<option></option>')
              .attr('value', card.multiverseid)
              .text(card.name)
              .data('card', card);
      });

      $('#cardlist').append(options);

      $.each(data, function(key, set) {
          $.each(set.cards, function(i, card) {
              card.imgUrl = "http://gatherer.wizards.com/Handlers/Image.ashx?multiverseid=" + card.multiverseid + "&type=card";
              mvidToCards[card.multiverseid] = card;
          });
      });

      $("form input[name='filter']").change(function () {
        var val = $(this).val();
        var found = options;

        if (val != "") {
          found = options.filter(function(option) {
            var txt = option.text().toLowerCase();
            return txt.indexOf(val) > -1;
          });
        }

        $('#cardlist option').detach(); // keep data
        $('#cardlist').append(found);

      }).keyup( function () { $(this).change(); });

      //
      // handler for color filters
      $('.color-filter').click(function(e){
        var color = $(e.target).attr('name').toLowerCase();
        $.each(cards, function(i, card) {
          if(card.colors){
            for(var i=0; i< card.colors.length; i++){
              if(card.colors[i].toLowerCase() === color) {
                $('#cardlist option[value="' + card.multiverseid + '"]').prop('disabled', !e.target.checked);
              }
            }
          }
        });
      });

      $("#cardlist").change(function(data) {
        var card = $("#cardlist option:selected").data('card');

        if(!card) {
          // nothing selected...
          return;
        }

        $('#preview')
          .attr('src', card.imgUrl)
          .on('dragstart', function(e) {
              this.style.opacity = "0.4";
              dragSrcEl = this;
              e.originalEvent.dataTransfer.effectAllowed = "move";
          })
          .on('dragend', function(e) {
              this.style.opacity = "1.0";
          });
      });
    });
  });

  function Table(root) {
    this.root = root;
    var cols = root.querySelectorAll("div.boardlist > div");
    console.log(cols);
    this.counts = root.querySelectorAll("div.card-headers span");
    var table = this;

    var cardColumns = {};
    var rowTypes = [];

    // Drop cards in particular columns
    [].forEach.call(cols, function(col) {
      var type = col.className.split(/\s/)[1];
      cardColumns[type] = $(col);
      rowTypes.push(type);

      col.addEventListener('dragover', handleDragOver, false);
      col.addEventListener('drop', function(e) {
        if (e.stopPropagation) { e.stopPropagation(); }
        var card = jQuery.data(dragSrcEl, "card");
        if (table.isMember(dragSrcEl)) {
          table.removeCard(dragSrcEl);
        }
        table.addCard(card, e.target.getAttribute('data-type'));
        return false;
      }, false);
    });

    this.cardColumns = cardColumns;
    this.rowTypes = rowTypes;
  }

  Table.prototype.rowTypes =
      ["creatures", "spells", "artifacts", "enchantments", "lands", "planeswalkers"];

  Table.prototype.rowTypes.forEach(function(thing) {
    Table.prototype[thing] = function() {
      return $("div." + thing + " img", this.root).map(function() {
        return jQuery.data(this, "card");
      }).toArray();
    }
  });

  Table.prototype.clear = function() {
    this.rowTypes.forEach(function(type) {
      var node = $("div." + type, this.root)[0];
      while (node.firstChild) {
        node.removeChild(node.firstChild);
      }
    });
    this.updateCounts();
  };

  Table.prototype.updateCounts = function() {
    var i = 0;
    var total = 0;
    var deck = this;
    this.rowTypes.forEach(function(type) {
      var len = deck[type]().length;
      deck.counts[i].innerHTML = "(" + len + ")";
      total += len;
      i++;
    });
    $("div.total")[0].innerHTML = "Total Cards: " + total;
  };

  Table.prototype.handleDragStart = function(e) {
    this.style.opacity = '0.4';
    dragSrcEl = this;
    e.dataTransfer.effectAllowed = 'move';
  };

  Table.prototype.handleDragEnd = function(e) {
    this.style.opacity = '1.0';
  };

  Table.prototype.handleDragOver = function(e) {
    if (e.preventDefault) { e.preventDefault(); }
    return false;
  };

  Table.prototype.setDeck = function(deck) {
    var thing = this;
    for (var section in this.cardColumns) {
      var column = this.cardColumns[section];

      deck[section].forEach(function(card) {
        thing.addCardToTarget(card, column);
      });
    }
    this.alignImages();
    this.updateCounts();
  };

  Table.prototype.addCard = function(card, loc) {
    var thing = this.cardColumns[loc];
    this.addCardToTarget(card, thing);
    this.alignImages();
    this.updateCounts();
  };

  Table.prototype.eachType = function(fun) {
    var deck = this;
    this.rowTypes.forEach(function(type) {
      fun(type, deck[type]());
    });
  };

  Table.prototype.isEmpty = function() {
    return this.cards().length == 0;
  }

  Table.prototype.cards = function() {
    var deck = this;
    return this.rowTypes.reduce(function(prev, curr) {
      return prev.concat(deck[curr]());
    }, []);
  }

  Table.prototype.addCardToTarget = function(card, loc) {
    var div = document.createElement('div');
    var img = document.createElement('img');
    var deck = this;

    div.className = "card";
    img.src = card.imgUrl;
    img.setAttribute("draggable", true);
    jQuery.data(img, "card", card);
    img.addEventListener('dragstart', this.handleDragStart, false);
    img.addEventListener('dragend', this.handleDragEnd, false);
    img.addEventListener('dragover', this.handleDragOver, false);
    img.addEventListener('drop', function(e) {
      if (e.stopPropagation) { e.stopPropagation(); }
      var card = jQuery.data(dragSrcEl, "card");
      var column = e.target.parentNode.parentNode;
      if(deck.isMember(dragSrcEl)) {
        deck.removeCard(dragSrcEl);
      }
      deck.addCard(card, column.getAttribute('data-type'));
      deck.alignImages();
      return false;
    }, false);
    $(div).append(img);
    loc.append(div);
  }

  Table.prototype.isMember = function(img) {
    return $(img).parents("div.boardlist")[0];
  }

  Table.prototype.removeCard = function(img) {
    var div = img.parentNode;
    div.parentNode.removeChild(div);
    this.alignImages();
    this.updateCounts();
  }

  Table.prototype.alignImages = function() {
    var maxidx = 0;

    $(".boardlist > *").each(function(i, e) {
      $(e).children().each(function(idx, img) {
          if(idx > maxidx) {
              maxidx = idx;
          }

          var pos = (idx * 25) + "px"
          $(img).css({ "position": "absolute",
                     "top": pos });
      });
    });

    var height = maxidx * 25 + 180;
    if (height < 500) {
      height = 500;
    }

    $("div.boardlist").css({"height": height + "px"});
  }
});
