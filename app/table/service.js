//this file exposes an API for express and socket.io
angular.module('table')
.service('tableService', function($http) {
  return {
    getTable: function(tableName, userName) {
      return $http.post('/api/table', JSON.stringify({ tableName: tableName, userName: userName }));
    },
    moveCard: function(tableName, userName, objectId, newX, newY, newZone, facedown) {
      return $http.put('/api/moveCard', JSON.stringify({ tableName: tableName, userName: userName, objectId: objectId, newX: newX, newY: newY, newZone: newZone, facedown: facedown }));
    },
    tapCard: function(tableName, userName, objectId, tapped) {
      return $http.put('/api/tapCard', JSON.stringify({ tableName: tableName, userName: userName, objectId: objectId, tapped: tapped }));
    },
    setLife: function(tableName, userName, newLife) {
      return $http.put('/api/setLife', JSON.stringify({ tableName: tableName, userName: userName, newLife: newLife}));
    },
    uploadDeck: function(tableName, userName, data) {
      return $http.post('/api/upload', JSON.stringify({ tableName : tableName, userName: userName, data: data }));
    },
    flipCard: function(tableName, userName, objectId, facedown) {
      return $http.put('/api/flipCard', JSON.stringify({ tableName : tableName, userName: userName, objectId: objectId, facedown: facedown }));
    },
  	setCounter: function(tableName, userName, objectId, counter) {
      return $http.put('/api/setCounter', JSON.stringify({ tableName: tableName, userName: userName, objectId: objectId, counter: counter }));
  	}
  };
}).factory('socket', function($rootScope) {
  var socket = io.connect();
  return {
    on: function(eventName, callback) {
      socket.on(eventName, function() {
        var args = arguments;
        $rootScope.$apply(function() {
          callback.apply(socket, args);
        });
      });
    },
    emit: function(eventName, data, callback) {
      socket.emit(eventName, data, function() {
        var args = arguments;
        $rootScope.$apply(function() {
          if (callback) {
            callback.apply(socket, args);
          }
        });
      })
    }
  };
});
