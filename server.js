//this file is responsible for configuring and launching node
var express = require('express');
var app = express();
var server = require('http').createServer(app);
var io = require('socket.io')(server);
var path = require('path');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');

mongoose.Promise = global.Promise; //to get rid of a deprecated promise warning
mongoose.connect('mongodb://localhost:27017/test');

app.use(express.static(path.join(__dirname, 'app')));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

require(path.join(__dirname, 'routes.js'))(app);
require(path.join(__dirname, 'socket.js'))(io);

server.listen(3000);
console.log('App is running on port 3000');
exports = module.exports = app;
