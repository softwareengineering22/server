//this file configures realtime socket.io events
module.exports = (io) => {
  io.on('connection', (socket) => {
    var roomName;

    socket.on('roomJoin', (room) => {
      roomName = room;
      socket.join(roomName);
    });

    socket.on('rollDice', (data) => {
      socket.to(roomName).emit('rollDice', data);
    });

    socket.on('disconnect', () => {
    });

    socket.on('sendMessage', (message) => {
      socket.to(roomName).emit('sendMessage', message);
    });

    socket.on('moveCard', (data) => {
      socket.to(roomName).emit('moveCard', data);
    });

    socket.on('tapCard', (data) => {
      socket.to(roomName).emit('tapCard', data);
    });

    socket.on('setLife', (data) => {
      socket.to(roomName).emit('setLife', data);
    });

    socket.on('flipCard', (data) => {
      socket.to(roomName).emit('flipCard', data);
    });

    socket.on('fixDeckCounters', () => {
      socket.to(roomName).emit('fixDeckCounters');
	});
	  
	socket.on('setCount', (data) => {
      socket.to(roomName).emit('setCount', data);
  
    });
  });
};
