//this file configures the routes that express uses to handle requests
var path = require('path');
var models = require(path.join(__dirname, 'app/table/table.js'));
var Table = models.table;
var Player = models.player;
var Card = models.card;

module.exports = (app) => {
  //Back-end API routes
  app.post('/api/table', (req, res) => {
    //set up test data
    if(req.body.tableName == 'test') {

      var charles = new Player({
        name: 'Charles',
        life: 15,
        poison: 7,
        cards: []
      });
      charles.cards.push(new Card({
        multiverseId: '423834',
        tapped: true,
        locX: 750,
        locY: 450,
        zone: 'battlefield'
      }));
      charles.cards.push(new Card({
        multiverseId: '383334',
        tapped: false,
        locX: 900,
        locY: 450,
        facedown: true,
        zone: 'battlefield'
      }));
      charles.cards.push(new Card({multiverseId: '378433', zone: 'deck'}));
      charles.cards.push(new Card({multiverseId: '394519', zone: 'deck'}));
      charles.cards.push(new Card({multiverseId: '423572', zone: 'hand'}));
      charles.cards.push(new Card({multiverseId: '423572', zone: 'hand'}));
      charles.cards.push(new Card({multiverseId: '423572', zone: 'hand'}));
      charles.cards.push(new Card({multiverseId: '253624', zone: 'graveyard'}));
      charles.cards.push(new Card({multiverseId: '373709', zone: 'exile'}));

      var james = new Player({
        name: 'James',
        life: -1,
        poison: 0,
        cards: []
      });
      james.cards.push(new Card({
        multiverseId: '370680',
        tapped: false,
        locX: 400,
        locY: 450,
        zone: 'battlefield'
      }));
      james.cards.push(new Card({
        multiverseId: '425810',
        locX: 400,
        locY: 600,
        zone: 'battlefield'
      }));
      james.cards.push(new Card({
        multiverseId: '420600',
        tapped: false,
        counter: 10,
        locX: 600,
        locY: 500,
        zone: 'battlefield'
      }));
      james.cards.push(new Card({multiverseId: '405135', zone: 'deck'}));
      james.cards.push(new Card({multiverseId: '380486', zone: 'deck'}));
      james.cards.push(new Card({multiverseId: '380508', zone: 'deck'}));
      james.cards.push(new Card({multiverseId: '380508', zone: 'deck'}));
      james.cards.push(new Card({multiverseId: '380500', zone: 'hand'}));
      james.cards.push(new Card({multiverseId: '380500', zone: 'hand'}));
      james.cards.push(new Card({multiverseId: '423565', zone: 'hand'}));
      james.cards.push(new Card({multiverseId: '11351', zone: 'graveyard'}));
      james.cards.push(new Card({multiverseId: '380473', zone: 'graveyard'}));
      james.cards.push(new Card({multiverseId: '409737', zone: 'exile'}));
      james.cards.push(new Card({multiverseId: '402048', zone: 'exile'}));

      Table.findOne({ name: 'test' }, (err, table) => {
        if(err) {
          console.log(err);
        }
        var newTable;
        if (table == undefined) {
          console.log('Constructing test table');
          var newTable = new Table({ name: 'test' });
          newTable.players.push(charles);
          newTable.players.push(james);
          table = newTable;
        }
        table.save((err, table) => {
          if(err) {
            console.log(err);
          }
          res.json(table);
        });
      });
    }
    //handle normal requests
    else {
      Table.count({ name: req.body.tableName }, (err, count) => {
        if(err) {
          console.log(err);
        }
        if(count == 0) {
          var table = new Table({ name: req.body.tableName });
          table.players.push({ name: req.body.userName });
          table.save((err, table) => {
            if(err) {
              console.log(err);
            }
            res.json(table);
          });
        }
        else if(count == 1) {
          Table.findOne({ name: req.body.tableName }, (err, table) => {
            if(err) {
              console.log(err);
            }
            if(table.players.length == 2) {
              if(table.players[0].name == req.body.userName || table.players[1].name == req.body.userName) {
                res.json(table);
              }
              else {
                res.status(403).send({
                  message: 'Two players already at this table: ' + table.players[0].name + ', ' + table.players[1].name
                });
              }
            }
            //if an initialized table doesn't have 2 players on it, the table needs to push second player only if that second player is not already there
            else if(table.players[0].name != req.body.userName) {
              table.players.push({ name: req.body.userName });
              table.save((err, table) => {
                if(err) {
                  console.log(err);
                }
                res.json(table);
              });
            }
            else {
              res.json(table);
            }
          });
        }
        else {
          console.log("There shouldn't be more than one table of a single name");
        }
      });
    }
  });

  app.put('/api/moveCard', (req, res) => {
    Table.findOne({ "name" : req.body.tableName, "players.name" : req.body.userName, "players.cards._id" : req.body.objectId}, (err, table) => {
      if(err) {
        console.log(err);
      }

      var playerIndex;
      if(table.players[0].name == req.body.userName) {
        playerIndex = 0;
      }
      else {
        playerIndex = 1;
      }
      var cards = table.players[playerIndex].cards;

      var cardIndex;
      for(var i = 0; i < cards.length; i++) {
        if(cards[i]._id == req.body.objectId) {
          cardIndex = i;
          break;
        }
      }

      var card = cards[cardIndex];
      card.locX = req.body.newX;
      card.locY = req.body.newY;
      card.zone = req.body.newZone;
      card.facedown = req.body.facedown;
      if(req.body.newZone != 'battlefield') {
        card.facedown = false;
        card.tapped = false;
        card.counter = 0;
      }

      table.players[playerIndex].cards[cardIndex] = card;
      //move the selected element to the end of the array so that it's drawn on top; mostly for persisting deck order
      table.players[playerIndex].cards.push(table.players[playerIndex].cards.splice(cardIndex, 1)[0]);
      table.save((err, table) => {
        if(err) {
          console.log(err);
        }
        res.json(table);
      });
    });
  });

  app.put('/api/tapCard', (req, res) => {
    Table.findOne({ "name" : req.body.tableName, "players.name" : req.body.userName, "players.cards._id" : req.body.objectId}, (err, table) => {
      if(err) {
        console.log(err);
      }

      var playerIndex;
      if(table.players[0].name == req.body.userName) {
        playerIndex = 0;
      }
      else {
        playerIndex = 1;
      }
      var cards = table.players[playerIndex].cards;

      var cardIndex;
      for(var i = 0; i < cards.length; i++) {
        if(cards[i]._id == req.body.objectId) {
          cardIndex = i;
          break;
        }
      }

      var card = cards[cardIndex];
      card.tapped = req.body.tapped;

      table.players[playerIndex].cards[cardIndex] = card;
      table.save((err, table) => {
        if(err) {
          console.log(err);
        }
        res.json(table);
      });
    });
  });

  app.put('/api/setCounter', (req, res) => {
    Table.findOne({ "name" : req.body.tableName, "players.name" : req.body.userName, "players.cards._id" : req.body.objectId}, (err, table) => {
      if(err) {
        console.log(err);
      }

      var playerIndex;
      if(table.players[0].name == req.body.userName) {
        playerIndex = 0;
      }
      else {
        playerIndex = 1;
      }
      var cards = table.players[playerIndex].cards;

      var cardIndex;
      for(var i = 0; i < cards.length; i++) {
        if(cards[i]._id == req.body.objectId) {
          cardIndex = i;
          break;
        }
      }

      var card = cards[cardIndex];
      card.counter = req.body.counter;

      table.players[playerIndex].cards[cardIndex] = card;
      table.save((err, table) => {
        if(err) {
          console.log(err);
        }
        res.json(table);
      });
    });
  });

  app.put('/api/flipCard', (req, res) => {
    Table.findOne({ "name" : req.body.tableName, "players.name" : req.body.userName, "players.cards._id" : req.body.objectId}, (err, table) => {
      if(err) {
        console.log(err);
      }

      var playerIndex;
      if(table.players[0].name == req.body.userName) {
        playerIndex = 0;
      }
      else {
        playerIndex = 1;
      }
      var cards = table.players[playerIndex].cards;

      var cardIndex;
      for(var i = 0; i < cards.length; i++) {
        if(cards[i]._id == req.body.objectId) {
          cardIndex = i;
          break;
        }
      }

      var card = cards[cardIndex];
      card.facedown = req.body.facedown;

      table.players[playerIndex].cards[cardIndex] = card;
      table.save((err, table) => {
        if(err) {
          console.log(err);
        }
        res.json(table);
      });
    });
  });

  app.put('/api/setLife', (req, res) => {
    Table.findOne({ "name" : req.body.tableName, "players.name" : req.body.userName}, (err, table) => {
      if(err) {
        console.log(err);
      }

      var playerIndex;
      if(table.players[0].name == req.body.userName) {
        playerIndex = 0;
      }
      else {
        playerIndex = 1;
      }

      table.players[playerIndex].life = req.body.newLife;
      table.save((err, table) => {
        if(err) {
          console.log(err);
        }
        res.json(table);
      });
    });
  });

  app.post('/api/upload', (req, res) => {
    Table.findOne({ name: req.body.tableName }, (err, table) => {
      if(err) {
        console.log(err);
      }

      var playerIndex;
      if(table.players[0].name == req.body.userName) {
        playerIndex = 0;
      }
      else if(table.players[1].name == req.body.userName) {
        playerIndex = 1;
      }

      var json = JSON.parse(req.body.data);
      var cards = [];
      json.cards.forEach(function(card) {
        cards.push(new Card({multiverseId: card, zone: 'deck'}));
      });

      table.players[playerIndex].cards = cards;
      table.save((err, table) => {
        if(err) {
          console.log(err);
        }
        res.json(table);
      });
    });
  });

  //Front-end Angular route
  app.get('*', (req, res) => {
    res.sendFile(path.join(__dirname, 'app/index.html'));
  });
};
