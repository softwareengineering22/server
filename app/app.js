//this file is responsible for initializing the angular application and configuring routing

angular.module('mtgipApp', ['ui.router', 'home', 'table', 'builder', 'ui.bootstrap'])
.config(function($stateProvider, $urlRouterProvider, $uibModalProvider) {
  $stateProvider
  .state('home', {
    url: '/',
    templateUrl: 'home/template.html'
  })
  .state('table', {
    url: '/table/{tableName}/{userName}',
    templateUrl: 'table/template.html',
    controller: 'TableCtrl',
    params: {tableName: null, userName: null}
  })

  .state('builder', {
    url: '/builder',
    templateUrl: 'builder/template.html',
    controller: 'BuilderCtrl'
  })
  .state('learn', {})
  .state('uploader', {})
  $urlRouterProvider.otherwise('/');
})
//This controller determines what buttons to show on the navbar based on state
.controller('NavCtrl', function($scope, $state, socket) {
  $scope.dieSelect = 2;
  $scope.currState = $state;
  $scope.$watch('currState.current.name', function (newValue, oldValue) {
      if(newValue.includes('table')) {
         $scope.showDice = true;
         $scope.showUploader = true;
      }
      else {
        $scope.showDice = false;
        $scope.showUploader = false;
      }
  });

  // this function is used to display only one die
  $scope.oneDie = function(){
    //Gets both dice from index.html
    var die1 = document.getElementById("die1");
    var die2 = document.getElementById("die2");
    //Hides the second dice
    die2.style.visibility="hidden";
    //Dropdown list for which type of dice you want to roll stays open till player selects a type of dice
    $('.dropdown').on({
        "shown.bs.dropdown": function() { this.closable = true; },
        "click":             function() { this.closable = false; },
        "hide.bs.dropdown":  function() { return this.closable; }
    });
    //Used to communicate through server to show which dice are hidden
    $scope.dieSelect = 1;
  }
  //this function displays two dice
  $scope.twoDie = function(){
    var die1 = document.getElementById("die1");
    var die2 = document.getElementById("die2");
    die2.style.visibility="visible";
    $('.dropdown').on({
        "shown.bs.dropdown": function() { this.closable = true; },
        "click":             function() { this.closable = false; },
        "hide.bs.dropdown":  function() { return this.closable; }
    });
    //Used to communicate through server to show which dice are hidden
    $scope.dieSelect = 2;
  }
  //Creates a scope value for each dice roll outcome.
  $scope.rollValue1;
  $scope.rollValue2;
  //Function rolls the dice via imported number based on type of dice chosen from dropdown list.
  $scope.rollDice = function(num) {
    //if there are two dice shown then...
    if($scope.dieSelect == 2) {
      //Create a random value between 1 and num for both dice.
      $scope.rollValue1 = Math.floor(Math.random() * num) + 1;
      $scope.rollValue2 = Math.floor(Math.random() * num) + 1;
      //dropdown list closes after dice selection
      $('.dropdown').on({
            "shown.bs.dropdown": function() { this.closable = true; },
            "click":             function() { this.closable = true; },
            "hide.bs.dropdown":  function() { return this.closable; }
      });
      //emit rollDice function and output values of rolls.
      socket.emit('rollDice', {'rollValue1' : $scope.rollValue1, 'rollValue2': $scope.rollValue2});
    }
    //if there is one die shown
    else if($scope.dieSelect == 1) {
      //Create random value for outcome of roll
      $scope.rollValue1 = Math.floor(Math.random() * num) + 1;
      $scope.rollValue2 = null;
      //dropdown list closes.
      $('.dropdown').on({
            "shown.bs.dropdown": function() { this.closable = true; },
            "click":             function() { this.closable = true; },
            "hide.bs.dropdown":  function() { return this.closable; }
      });
      //output answer for one die roll outcome.
      socket.emit('rollDice', {'rollValue1' : $scope.rollValue1});
    }
  };

  // socket event for function roll dice
  socket.on('rollDice', function(data) {
    //if the data for the second dice rolled is not empty then die one and die two are given a roll value.
    if(data.rollValue2 != undefined) {
      $scope.rollValue1 = data.rollValue1;
      $scope.rollValue2 = data.rollValue2;
      $scope.twoDie();
    }
    else {
      $scope.oneDie();
      $scope.rollValue1 = data.rollValue1;
    }
  });
})

//this is to enable the learn modal to show up in any state
.run(function($rootScope, $uibModal) {
  $rootScope.$on('$stateChangeStart', function (event, toState) {
    if(toState.name == 'learn') {
      $uibModal.open({
          templateUrl : 'learn/modal.html',
          size: 'xl'
      });
      event.preventDefault();
    }
    else if(toState.name == 'uploader') {
      $uibModal.open({
          templateUrl : 'table/upload.html',
          controller: 'TableCtrl',
          size: 'sm'
      });
      event.preventDefault();
    }
  })
});
