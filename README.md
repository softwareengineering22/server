# README #
  
This is an application functions as a sandbox interface to play Magic the Gathering in your browser with minimal setup.  
  
It runs on the MEAN stack with socket.io.  
  
In order to run have mongodb and node installed.  
Run npm install in the repository to install required dependancies for node.  
Run mongod if you have the default install location for your database file.  
Run node server in the directory that the server is located in.  
Go to 127.0.0.1:3000  
  
  
##The basic file and directory organization is as follows:  
  
  
###root contains:  
**server.js**: gets node configured and the server as an express app  
**routes.js**: configures the paths that express uses to interact with the front-end and back-end  
**socket.js**: configures the server-side logic that happen on socket event  
**package.json**: standard node file; defines dependencies and the main file to run   
**app**: this directory stores the most of the web application; all front end content should go here  
**.gitignore**: this file excludes node_modules from being updated to the git repository
  
###app contains:  
**index.html**: this file is the base markup for every page on the site; content from templates goes into the div class="view-container  
**style.css**: sitewide stylesheet  
**app.js**: the base angular module; this file handles routing for the front-end  
###each subdirectory in app is responsible for providing the resources for each function of the page  
**template.html**: this markup the will define the page; it will be injected into the index.html page  
**component.js**: this file contains the javascript logic for its template.html page