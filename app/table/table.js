//this file defines the model that a table will use in the database
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var CardSchema = new Schema({
  multiverseId: {type: String, required: true},
  tapped: {type: Boolean, default: false},
  counter: {type: Number, default: 0},
  locX: Number,
  locY: Number,
  facedown: {type: Boolean, default: false},
  zone: {type: String, required: true}
});

var PlayerSchema = new Schema({
  name: String,
  life: {type: Number, default: 20},
  poison: {type: Number, default: 0},
  cards: [CardSchema]
});

var TableSchema = new Schema({
  name: {type: String, index: true, unique: true, required: true},
  players: [PlayerSchema]
});

module.exports = {
  table: mongoose.model('Table', TableSchema),
  player: mongoose.model('Player', PlayerSchema),
  card: mongoose.model('Card', CardSchema)
}
